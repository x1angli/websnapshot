# project/main/views.py


#################
#### imports ####
#################
import base64
from flask import render_template, Blueprint, request, jsonify, url_for
import os
import subprocess

################
#### config ####
################

main_blueprint = Blueprint('main', __name__, )


################
#### routes ####
################


@main_blueprint.route('/')
def home():
    return render_template('index.html')


@main_blueprint.route("/about/")
def about():
    return render_template("main/about.html")


@main_blueprint.route('/shoot/', methods=['POST', 'GET'])
def shoot():
    dic = request.form
    mUrl = dic.get("url")
    path = os.path.dirname(os.path.realpath(__file__)) + "/shoot.py"
    print("python " + path + " " + mUrl)
    subprocess.check_output("python " + path + " " + mUrl, shell=True)
    u = "http://" + mUrl + "/"
    u = base64.encodestring(u)
    u = u[:-2]
    # u = '../media/' + u + '.png'
    # u = url_for("media", filename=u)
    # return u
    return jsonify(url='static/cache/' + u + '.png')
